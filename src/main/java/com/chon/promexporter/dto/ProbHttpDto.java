package com.chon.promexporter.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ProbHttpDto {
    private Integer responseCode;
    private String responseStatus;
    private Double responseTime;
    private Long certificateExpired;
    private Long certificateExpiredIn;
    private Integer isCertificateExpired;
    private String targetIpAddress;
}
