package com.chon.promexporter.controller;

import com.chon.promexporter.dto.ProbHttpDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateExpiredException;
import java.security.cert.X509Certificate;
import java.util.Date;

@Controller
@Slf4j
public class HttpRequestExporterController {

    @GetMapping(value = "/prob-http", produces = MediaType.TEXT_PLAIN_VALUE)
    public @ResponseBody String getToUrl(@RequestParam String target) {
        log.info("Target: {} {}",target,this.getIp(this.getHostName(target) ));


        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
            @Override
            public boolean hasError(HttpStatus statusCode) {
                return false;
            }
        });
        int status = 0;
        int statusCode = 0;
        long certificateExpired = 0;
        long certificateExpiredIn = 0;
        int isCertificateExpired = 1;
        Double responseTime = 0d;
        String hostIpAddress = "";
        try {
            long startTime = (new Date()).getTime();
            ResponseEntity<String> response = restTemplate.getForEntity(target , String.class);
            responseTime = (Long.valueOf( (new Date()).getTime()-startTime) ).doubleValue()/1000;
            if(response.getStatusCode() == HttpStatus.OK){
                status = 1;
                statusCode = response.getStatusCodeValue();
            }
            hostIpAddress = this.getIp(this.getHostName(target) );

            certificateExpired = this.testConnectionTo(target);
            if(certificateExpired > (new Date()).getTime()){
                isCertificateExpired = 0;
                certificateExpiredIn = certificateExpired-(new Date()).getTime();
            }


        }catch (Exception e){
            log.error(e.toString());
        };


        return generateResponse(
                ProbHttpDto.builder()
                        .responseCode(statusCode)
                        .responseStatus(String.valueOf(status))
                        .certificateExpired(certificateExpired)
                        .certificateExpiredIn(certificateExpiredIn)
                        .isCertificateExpired(isCertificateExpired)
                        .responseTime(responseTime)
                        .targetIpAddress(hostIpAddress)
                        .build()
        );
    }

    private String getHostName(String target) {
        try {
            URL url = new URL(target);
            return url.getHost();
        }catch (Exception ignore){}
        return null;
    }

    private String getIp(String host){
        try {
            InetAddress address = InetAddress.getByName(host);
            return address.getHostAddress();
        }catch (Exception ignore){}
        return null;
    }

    private String generateResponse(ProbHttpDto probHttpDto){
        String response = "";

        response += "certificate_expired_in " + probHttpDto.getCertificateExpiredIn() + "\n";
        response += "certificate_expired_time " + probHttpDto.getCertificateExpired() + "\n";
        response += "certificate_expired " + probHttpDto.getIsCertificateExpired() + "\n";
        response += "response_time " + probHttpDto.getResponseTime() + "\n";
        response += "response_status{ip=\"" + probHttpDto.getTargetIpAddress() + "\"} " + probHttpDto.getResponseStatus() + "\n";
        response += "response_code " + probHttpDto.getResponseCode();

        return response;
    }

    private long testConnectionTo(String aURL) throws Exception {
        URL destinationURL = new URL(aURL);
        if(destinationURL.getProtocol().equals("https")){
            HttpsURLConnection conn = (HttpsURLConnection) destinationURL.openConnection();
            conn.connect();
            Certificate[] certs = conn.getServerCertificates();
            long expireTime = 99999999999999999l;
            for (Certificate cert : certs) {
                if(cert instanceof X509Certificate) {
                    try {
                        ( (X509Certificate) cert).checkValidity();

                        if(expireTime > ( (X509Certificate) cert).getNotAfter().getTime()){
                            expireTime = ( (X509Certificate) cert).getNotAfter().getTime();
                        }
                    } catch(CertificateExpiredException cee) {
                        log.info("Certificate is expired");
                    }
                }
            }
            return expireTime;
        }else{

        }
        HttpsURLConnection conn = (HttpsURLConnection) destinationURL.openConnection();
        conn.connect();
        log.info(String.valueOf(conn.getResponseCode()));
        log.info(conn.getContent().toString());
        
    }

}
