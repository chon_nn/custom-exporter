package com.chon.promexporter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PromExporterApplication {

    public static void main(String[] args) {
        SpringApplication.run(PromExporterApplication.class, args);
    }

}
